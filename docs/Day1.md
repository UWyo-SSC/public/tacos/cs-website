---
id: 'day1'
title: 'Day 1: Introduction to Computer Science and the Micro:bit'
---
import useBaseUrl from '@docusaurus/useBaseUrl';

<iframe width="560" height="315" src="https://www.youtube.com/embed/Jso9bhh83Xc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to day 1 of The Artful Craft of Science! Today we are going to take the first steps in discovering the best parts of computer science. We are going to start with an introduction to computer science and the Micro:bit, and create our first Micro:bit program. Then, we'll introduce the sensor:bit and the sensors and try them out! 

## What is Computer Science?

<iframe width="560" height="315" src="https://www.youtube.com/embed/KBYUs4gXAJ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Computer science is... you guessed it! The study of computers and how to _compute_ things. To _compute_ something means to determine something, oftentimes by using math. For example, you can _compute_ the answer to 2+2 in your head! A _computer_ is a device we use to _compute_ the answers to a lot of different problems!

All computers do four basic things:
1. Gather Input
2. Store Data
3. Process the Data
4. Produce Output

An **input** is a method to give the computer information. One example of an **input** is typing out a text on your cell phone to a friend!

An **output** is a method to get information from the computer. One example of an **output** is the text message that appears on your screen from a friend!

Watch this video to learn a little bit more about computers (look out for those four basic things!).

<iframe width="560" height="315" src="https://www.youtube.com/embed/mCq8-xTH7jA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

So, computers take **input**, store it as **data**, **process** the data, and **output** the result!

Humans can be computers too! Look at the math problem `4 * 11 = ` and talk about how a human _computes_ the answer. First, you **gather input** by reading the problem and **store the data** in your head.  Then, you **process the data** and figure out the solution. Next, you **output** the solution by writing it down or speaking it out loud.

## What is a Micro:bit?

<iframe width="560" height="315" src="https://www.youtube.com/embed/fQSSLxbMC9w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In our activities this week, we will use a Micro:bit, a Sensor:bit, and some Octopus sensors. For now, we will focus on the first of these devices. The Micro:bit is a small computer that has several different **inputs** and **outputs**.

Watch this short video that gives an introduction to a few of the Micro:bit's features.

The Micro:bit has these **inputs**:
1. Buttons (A, B, and Reset)
2. Bluetooth Radio
3. Three Analog Pins (these can be used for a lot of things!)
4. USB
5. Temperature Sensor
6. Light Sensor
7. A Compass
8. An Accelerometer (it reads how you move the Micro:bit through space)

And these **outputs**:
1. 25 LED lights
2. Bluetooth Radio
3. Three Analog Pins
4. USB

You may have noticed that the Bluetooth Radio, Analog Pins, and USB connection are both inputs _and_ outputs! 

In order to use the Micro:bit, we need to tell it what to do. Giving a computer instructions on how to handle input and output is called **programming** the computer. Let's get started!

## Activity: Hello Micro:bit

<iframe width="560" height="315" src="https://www.youtube.com/embed/5RrwCMyh3XI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

We're going to **program** the Micro:bit to tell us "_Hello_" and "_Goodbye_"! We'll do this in five steps:

1. Create a new project
2. Get familiar with Makecode
3. Write the program
4. Flash the Micro:bit
5. Test your code

### Setup
For this activity, you will need: 
- 1 micro:bit
- micro:bit cable

### Step 1 - Creating our First Project

Let's head to [makecode.microbit.org/v4](https://makecode.microbit.org/v4) to start programming.  We suggest using Chrome because it provides some additional features that we'll use this week.  Once you've followed the link, make sure that you're using v4 by checking for the "v4" in the page URL. You should see a page that looks like this:

![Makecode Landing Page](/pictures/day1/makecode-main-page-v4.jpg)

Click the purple square that says "New Project"

![New Project](/pictures/day1/new-proj.png)

Give your project a name, something like "Hello Micro:bit" should do the trick, and click "Create".

### Step 2 - Getting Familiar with the Makecode Editor

The Micro:bit can be programmed using a block-based programming language. You should be looking at a page with two `Blocks`, the `on start` and the `forever` blocks.  We are going to use these and other blocks to tell the Micro:bit what to do! 

Any blocks you place within the `on start` block will run when the Micro:bit turns on or resets, while any blocks inside the `forever` block will run over and over as long as the Micro:bit is on! We won't use `on start` or `forever` in our first program, but they will come in handy in the future! 

On the far left of the screen, you should see a virtual Micro:bit.
![Micro:bit Emulator](/pictures/day1/microbit-emulator.png)
The virtual Micro:bit lets you see how your program will behave before you download it to your own physical Micro:bit. Each time you add, change, or remove a block, the virtual Micro:bit will restart and run the code that is in the editor.

To the right of the virtual Micro:bit, there are several category tabs that hold all of the possible blocks we can use to program the Micro:bit
![Block Categories](/pictures/day1/block-categories.png)
Click on the different categories and see some of the different blocks we have to use!

The main portion of our screen is the Makecode Editor. This is where we will place blocks that we are using to program our Micro:bit.
![Makecode Editor](/pictures/day1/makecode-editor.png)

As we write programs, you'll notice that different blocks have different shapes.  We can attach blocks together only if their shapes match, just like puzzle pieces!

### Step 3 - Writing Our First Program

Now that we've gotten familiar with the Makecode site, let's write out first program! We're going to use the **Button A** and **Button B** inputs and the **LED** outputs to display messages!

#### Step 3a: Inputs

The first thing we need to do is tell the Micro:bit which inputs it should look for. Click on the `Input` category to bring up a list of the different input blocks we can use, and drag two `on button A pressed` blocks into the editor. To make an `on button B pressed` box, click on the `A` dropdown and change it to `B` so we have two blocks, one for each button.
<!-- ![Button A and Button B](/videos/day1/buttonab.gif) -->
Now, whatever we want the computer to do when we push `Button A`, we can put inside that block, and whatever we want to happen when we push `Button B`, we will put in that block!

#### Step 3b: Outputs

Let's have Micro:bit tell us hello when we push `Button A`, and goodbye when we push `Button B`.  To do that, lets' go to the "Basic" category and drag the `show string` block into each of our button blocks. In programming, we call blocks of text "strings".
<!-- ![Moving String Blocks](/videos/day1/string-blocks.gif) -->

Now, we can replace the text inside of our new blocks with whatever text we want! Change the text so that when we push `Button A`, the Micro:bit will show "Hello Jane", but using your own name. Again, change the text so that when we push `Button B`, the Micro:bit will show "Goodbye Jane", but with your name.
<!-- ![Typing Your Own Name](/videos/day1/hello-goodbye.gif) -->

**Congratulations!!** You just wrote your very first program for the Micro:bit. Let's use the virtual Micro:bit on the left to see if our program is working the way we expect it to. Click on the `A` and `B` buttons and see if the virtual Micro:bit displays the greeting and farewell correctly.
<!-- ![Checking the Virtual Micro:bit](/videos/day1/virtual-test.gif) -->

It looks like our program is running the way we expect it to, so let's download our program onto our physical Micro:bit and see if it works!

It's good practice to check your code before downloading it - this can save you some difficult debugging in the future! That's why **Solution** code will be hidden in a **drop-down spoiler** right before the flashing step for this and all future activities. If you get stuck, you can always compare your code to the solution!

<details>
  <summary>Hello Microbit Solution Code!</summary>

  ![Micro-USB into the Micro:bit](/pictures/day1/day1_sol.png)
  
</details>

### Step 4 - Flashing the Micro:bit

Putting code onto the Micro:bit is called **flashing**, and it can work a little different depending on the type of computer and browser you are using. Regardless, the first thing you'll need to do is plug the small end of the USB cable you were given (micro USB) into the top of your Micro:bit, and plug the other end (USB) into your computer.
![Micro-USB into the Micro:bit](/pictures/day1/usb.jpg)

#### Chrome Shortcut (Preferred Method)

If you are using a recent version of the Chrome browser, you can connect your Micro:bit to Makecode and then just press the "Download" button. Your program will automatically be downloaded onto the Micro:bit! To do this, click the three dots to the right of the download button, and select, "connect device," and follow the on-screen instructions.
[Linking Micro:bit to Makecode](/pictures/day1/connectdevice.png)

If you are struggling to connect your micro:bit, follow the steps that MakeCode suggests:

![Micro:bit connection failed](/pictures/day1/connect-failed.jpg)

First, check your connection - there should be a yellow light that appears on the back of the micro:bit when it's connected to your computer. If you don't see a yellow light, try a different cable. If the connection seems good, you probably need to update the firmware on your micro:bit. To update the firmware, follow the directions at [https://makecode.microbit.org/device/usb/webusb/troubleshoot](https://makecode.microbit.org/device/usb/webusb/troubleshoot).

<!-- (TODO if time - We've got a video tutorial for updating firmware, linked in the description below.) -->

#### Flashing with All Browsers

Once your Micro:bit is plugged in, click the `Download` button on the bottom left-hand corner of the screen. This will download the program onto your computer as a `.hex` file. Now, all you need to do is drag and drop the `.hex` file onto the Micro:bit. 

Depending on whether you are using Windows, Mac, a Chromebook, or other type of computer, this will look slightly different, but the idea is the same. Find the downloaded `.hex` file in your `Downloads` folder, and find the Micro:bit folder into your file system. Then, drag the `.hex` file onto the Micro:bit just like you would if the Micro:bit was a USB Flash Drive.
<!-- TODO ![Download Hex File](/videos/day1/download-hex.gif) -->

Your Micro:bit's yellow light on the back will flash repeatedly while the program is downloaded. Once the program has finished downloading, the yellow light will glow solid. 

#### Troubleshooting

If you are having trouble downloading your program onto the Micro:bit, visit this site to get some help: [Flashing the Microbit Support](https://support.microbit.org/support/solutions/articles/19000013986-how-do-i-transfer-my-code-onto-the-micro-bit-via-usb)

You can visit these pages for specific help based on your browser. For **Windows Browsers** visit these pages: [Microsoft Edge](https://makecode.microbit.org/device/usb/windows-edge_), [Internet Explorer](https://makecode.microbit.org/device/usb/windows-ie), [Chrome (Windows)](https://makecode.microbit.org/device/usb/windows-chrome), [Firefox (Windows)](https://makecode.microbit.org/device/usb/windows-firefox). For **Mac Browsers** visit these pages: [Safari](https://makecode.microbit.org/device/usb/mac-safari), [Chrome (Mac)](https://makecode.microbit.org/device/usb/mac-chrome), [Firefox (Mac)](https://makecode.microbit.org/device/usb/mac-firefox).

### Step 5 - Test your Code!

Once the program is downloaded, let's try it out! Press the `A` and `B` buttons to see the text that you wrote scroll across the screen!

**Wooo!** We programmed the Micro:bit computer to take an **input**, a button press, and give us **output**, text scrolling across the LEDs. Inside the Micro:bit, a lot of things have to be **processed** in order to produce that text on the LED display, it needed to figure out what LEDs to use, and when to turn them on and off, and how long to wait before turning them on or off.

Feel free to click the `Home` button in the top left, and try out some of the tutorials that Makecode offers! You could also go back into your code and play with other outputs you might be able to assign to the `A` and `B` buttons (did you notice you can make pictures with the LEDs??).

## Sensor:bit and Octopus Sensors
<iframe width="560" height="315" src="https://www.youtube.com/embed/aBKQMciHl8E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

![sensors and board](/img/sensors-and-board.png)

The Elecfreaks Sensor:bit is an expansion to the micro:bit. We can plug our micro:bit into this board to gain additional inputs and outputs. It contains:

![sensor bit audio jack](/pictures/day1/sensor-bit-audio-jack.png)
- an audio jack for plugging in headphones

![sensor bit buzzer](/pictures/day1/sensor-bit-buzzer.png)
- a buzzer that will allow us to use sounds in our programs

![sensor bit GVS port](/pictures/day1/sensor-bit-gvs.png)
- a 16-channel standard GVS port that we can plug our sensors into 

![sensor bit I2C](/pictures/day1/sensor-bit-I2C.png)
- and an I2C Port for controllers and peripheral devices 

We won't use the I2C port this week, but we will use everything else! The audio jack and buzzer will allow us to hear the sounds we use in our programs. The GVS port on the sensor:bit will allow us to connect sensors to our micro:bits. We have three sensors: 

![noise sensor](/pictures/day1/noise-sensor.jpg)
- Elecfreaks Octopus Analog Noise Sound Sensor Detection Module  

![UV sensor](/pictures/day1/uv-sensor.jpg)
- Elecfreaks Octopus Analog UV sensor  

![temperature and humidity sensor](/pictures/day1/temp-humidity-sensor.jpg)
- Elecfreaks Octopus Temperature And Humidity Sensor  

These sensors do exactly what it sounds like they do; the measure noise, UV light, temperature, and humidity. These additional inputs will allow us to use information from the environment around us in our programs. For example, we could use the **noise sensor** to make a clap on/clap off light, we could use the **UV sensor** to remind us to put sunscreen on when we go outside in the sun, or we could use the **temperature and humidity sensor** to track the weather!  

### Hardware Setup
To use the sensors, first we need to assemble everything. 

![separated components](/pictures/day1/components-labeled.jpg)

Start by plugging in the micro:bit by sliding the micro:bit's pins into the slot on the sensor:bit connector. Try to keep it nice and even as you push the micro:bit down. These devices can be a bit fragile, so be careful as you fit it into place.

![plug in micro:bit](/pictures/day1/slot-small.gif)

Now, we can connect our sensors! Take a look at the black, yellow, and red cables. Notice that one end has a clip to secure it in place, and the other end is flat. 

![clip and flat sides of cable](/pictures/day1/svg-cable-web.jpg)

The clip side plugs into your sensors. Go ahead and plug a cable into each sensor, like this:

![attach sensor to cable](/pictures/day1/sensor-plug-web.gif)

Then, you can plug each sensor into the sensor:bit. Make sure the colors of the SVG cable match the colors on your board! Plug the noise sensor into pin 1, the UV sensor into pin 2, and the temperature/humidity sensor into pin 8. 

![components assembled](/pictures/day1/sensors-attached-web.jpg)

### Useful links to documentation
When you're learning about new gadgets and coding tricks, one of the best places to go when you have questions is the online wiki! These include MakeCode examples for each component, which are very helpful when you're just starting out! We'll have them linked in our video descriptions on YouTube in case you need additional help with the sensor:bit, noise sensor, UV sensor, and temperature and humidity sensor. 
[Elecfreaks Sensor:bit](https://www.elecfreaks.com/learn-en/microbitExtensionModule/sensor_bit.html)  
[Elecfreaks Octopus Analog Noise Sound Sensor Detection Module](https://www.elecfreaks.com/learn-en/microbitOctopus/sensor/octopus_ef04081.html)  
[Elecfreaks Octopus Analog UV sensor](https://www.elecfreaks.com/learn-en/microbitOctopus/sensor/octopus_ef04093.html)  
[Elecfreaks Octopus Temperature And Humidity Sensor](https://www.elecfreaks.com/learn-en/microbitOctopus/sensor/octopus_ef04019.html)  

## Activity: Sensor Tester
<iframe width="560" height="315" src="https://www.youtube.com/embed/QHOQ0NKpwv0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Now that our hardware is all set up, let's test it out! 

### Setup
For this activity, you'll need: 
- Micro:bit and cable
- Sensor:bit (with the micro:bit plugged in)
- Noise sensor (plugged into P1)
- UV sensor (plugged into P2)
- Temperature/Humidity sensor (plugged into P8)
- A battery pack

### Step 1: Create a new project and Add the IoT Environment Kit Extension
In Chrome, head to [makecode.microbit.org/v4](https://makecode.microbit.org/v4) (don't forget we'll be using version 4 all week!) and create a new project. Name your project "sensor-tester." 

Whenever we want to use our sensors, we need to add the iot-environment-kit extension. Open the `advanced` tab, scroll all the way bottom, and click on `extensions`. In the search bar, type in "iot" and hit enter. Then, click the "iot-environment-kit" extension to add it.

![add IoT extension](/pictures/day1/iot-extension.gif)

Now we have new menu items in MakeCode! The one that we'll use is called `Octopus`. When you open this menu, you'll see a long list of blocks that we can use. Most of these blocks are for sensors we don't have, but three of these blocks will be very important this week: the `value of noise(dB) at pin P1` and `UV sensor P1 level (0-15)` blocks in the Octopus menu, and the `value of dht11 temperature (°C) at pin P15` in the "more" menu that appears below the Octopus menu. Let's try it out!

### Step 2: Test the Noise Sensor
First, let's test out our noise sensor!

We are going to use a special block to graph real-time data from our noise sensor. Expand the `Advanced` menu, and open the `Serial` tab. Find the `serial write value "x" = 0` block and drag it into your forever block. Once you have this blocks in place, you'll see a new button appear below your virtual micro:bit that says, `Show Console Simulator`.

![noise block](/pictures/day1/serial-write-block.jpg)

Open the `Octopus` tab, and look for the `value of noise(dB) at pin P1` block (it's about halfway down). Drag this block into the editor and use it to replace the `0` in your `serial write value "x" = 0` block.

![noise block](/pictures/day1/serial-write-noise.jpg)

Finally, click on the `"x"` in this box and type in "noise (dB)". Your code should look like this: 

![noise-logger](/pictures/day1/noise-logger.png)

Our code is finished, now all we need to do is test it out!

Make sure your micro:bit is connected to Chrome, and then flash your code. If your Micro:bit is connected to Chrome, you should see another new button appear below the virtual micro:bit called `Show Console Device`. If you click on that button, you should see a graph plotting real-time noise data, directly in your browser! *Note: This can be a bit finicky. If it's not working, try refreshing the page or unplug and replug your micro:bit into the computer.*

Take some time to play with being as quiet and as loud as possible. Without touching the sensor, what's the lowest number you can get? How about the highest? Each sensor may be a little bit different, so take note of your readings because you'll want to know these numbers in programs we write later. For example, the lowest number my sensor is reaching is 43 dB, and the highest is about 90 dB, but those numbers might be different for you. 

![noise logged](/pictures/day1/noise-logged.png)

### Step 3: Test the Temperature/Humidity Sensor
#### Step 3a - Try the Temperature Sensor
Now that you've had some time to explore the noise sensor, let's move on to the Temperature/Humidity sensor. This will work exactly the same as the noise sensor, so we just need to update the `serial write value "x" = 0` block so that it plots temperature/humidity instead of noise. 

Open the `Octopus` tab, and then open the `more` menu that appears below the `Octopus` tab. Look for the `value of dht11 temperature (°C) at pin P15` block at the bottom of the list. Drag this block into the editor and use it to replace the `value of noise(dB) at pin P1` part of in the `serial write value "x" = 0` block.

![replace noise](/pictures/day1/replace-noise.jpg)

Our Temperature/Humidity sensor is not plugged into pin 15. We have it plugged into pin 8. So, click the `P15` part of this block and change it to `P8`. *This is also a good time to double check that your temperature/humidity sensor is plugged into pin 8!* Look at the other dropdown in this block. Our sensor provides data on temperature in Celsius (C) or Fahrenheit (F), and humidity (measured from 0~100).

![temperature/humidity sensor options](/pictures/day1/temp-sensor-options.png)

Let's start with temperature in degrees Fahrenheit. Select `temperature(°F)` from the drop-down menu. Finally, change the "noise (dB)" part of the block to "temperature (F)". Your code should look like this: 

![temp F logger](/pictures/day1/temp-f-logger.png)

Flash this code to your micro:bit to test it out! Watch the graph while you hold the sensor in your hand to heat it up, and then let it go and so it cool back down again. Notice that it heats up slowly, and cools down even slower.

![temp F heating](/pictures/day1/temp-f-heat.png)
![temp F cooling](/pictures/day1/temp-f-cool.png)

#### Step 3b - Try the Humidity Sensor
Let's test our humidity sensor next. In the `value of dht11` block, open the drop-down menu and select `humidity(0~100)`, and change the "temperature (F)" to "humidity" so that your code looks like this: 

![humidity logger](/pictures/day1/humidity-logger.png)

Flash your code and return to the graph. Try breathing on the humidity sensor like you're trying to fog up a mirror - it's pretty reactive! Then try blowing on it like you're trying to cool down a spoonful of hot soup. 

![humidity graph](/pictures/day1/humidity-graph.png)

### Step 4: Test the UV Sensor
Let's move on to our final sensor, the UV Sensor! We'll test this sensor a bit differently because the UV sensor requires sunlight to work, so we probably won't be able to leave the micro:bit plugged into our computer. We'll have to go without the graph for this one. 

Go ahead and drag the `serial write value` block out of the `forever` block, and put a `show number 0` block from the `basic` tab into your forever block instead. 

![show number block](/pictures/day1/show-number.jpg)

Now, there are two ways we can show UV values on our micro:bit. In the `Octopus` tab, there is a `UV sensor P0 level (0~15)` block that works. However, the Elecfreaks Wiki example uses an `analog read pin P0` block instead, so we'll use that. Expand the `Advanced` drop-down, and open the `pins` menu. Find the `analog read pin P0` block, and drag it in to replace the `0` in the `show number 0` block. 

![analog pin block](/pictures/day1/analog-pin-block.jpg)

Our UV sensor should be plugged into pin 2, so change `P0` to `P2`. *Remember to double check that your UV sensor is actually plugged in to pin 2!* Your code should look like this: 

![UV value display](/pictures/day1/uv-val-display.png)

Flash your micro:bit, and then unplug it from the computer, and plug it into a battery pack. You can plug the battery pack in by pushing the white plug into the port in the top-left of the micro:bit. There's a groove that tells you which way to plug it in. 

![Battery groove](/pictures/day1/battery-groove-sm.jpg)

Be sure to handle these components very carefully because they're a bit delicate. When you're ready to unplug the battery pack, make sure to pull from the white plastic bit. Don't just yank on the cables!

![battery pack](/pictures/day1/battery-pack-42.gif)

Now you can experiment with the UV sensor on the go! You could test it out next to a window, outside in the sun and in the shade. Remember to pay attention to the numbers! When we tested ours on a sunny day, our UV sensors showed a 1 or 2 inside and up to 315 outside, ranging from 35-80 in the shade. How does yours compare? 

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/Crn2AAgYmUQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Great job today, we covered a lot! We learned that computer science is the study of computers and how to _compute_ things. We learned that all computers do four basic things: gather input, store data, process the data, and produce output. We learned how to program the micro:bit using MakeCode and how to connect the sensor:bit and sensors. Then, we finished the day off by testing all of our sensors! Come back tomorrow, to learn about variables and how micro:bits communicate! 