---
id: 'day3'
title: 'Day 3: Boolean and Control Flow'
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/CeYGZE2-BPg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to day 3 of TACoS Computer Science! Today, we'll learn all about booleans, if statements, and loops. We'll practice using booleans and if statements by making an automatic bathroom fan. To finish out the day, we'll practice using loops by creating a program to feed a big orange cat. 

## Boolean Expressions

<iframe width="560" height="315" src="https://www.youtube.com/embed/9IrFHy6Efjw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

A **boolean expression** is an expression that always evaluates to either **true** or **false**. Let's look at an example:

`10 < 5`

This **boolean expression** asks "is 10 less than 5?" and evaluates to **false**!  You might be familiar already with the greater than, greater than _or_ equal to, less than, and less than _or_ equal to symbols. Here's a list of all the symbols we use to create **boolean expressions** while programming!

`<` - Less than

`>` - Greater than

`<=` or `≤` - Less than _or_ equal to

`>=` or `≥` - Greater than _or_ equal to

`==` or `=` - Equal to

`!=` or `≠` - _Not_ equal to

Although the `≤`, `≥` and `≠`symbols are easy to write on paper, we aren't able to type them on a keyboard! Because many programming languages are typed, you will often see `<=`, `>=`, and `!=` used instead.  In Makecode, we'll see the first set of symbols already in the blocks, we don't have to do any typing! Also, many programming languages use `==` instead of `=` to compare two values for equality because they use the `=` to set a variable equal to a value. In Makecode, we use the `set variable` block to set a variable equal to a value, so the `=` symbol works to check for equality.

The values on each side of a **boolean expression** can be numbers, like in the expression `125 >= 32` (which evaluates to true!) or they can be math expressions like `7*5 == (7*counter)` (which evaluates to true if `counter` has the value of 5, and false if it is anything else). 

One of the primary reasons **boolean expressions** are useful is to help _control the flow_ of our programs.

## Control Flow: If Statements

<iframe width="560" height="315" src="https://www.youtube.com/embed/RHssrGtbhqA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

_Control Flow_ is the path your program takes when it is being run.  Often, we want different things to happen when certain _conditions_ are met. For example, we might use an **if statement** to use humidity data to control an automatic bathroom fan. _If_ the Micro:bit senses that the bathroom is really humid, _then_ the Micro:bit should turn on the fan, _else if_ the Micro:bit senses that it is not humid, _then_ it should turn off its fan. The humidity level is our _condition_, and there are two paths our program can take. It can turn on the fan or turn off the fan. 

If statements are what we use in programming to choose between two or more different paths. There are three main parts to if statements:
- `If` - This is a statement that tells the computer what condition to look for; if that condition is met, then it completes its task
- `else if` - This is a statement that gives the computer a new condition to look for if the previous conditions were not met. If the else if condition is met, then it completes its task.
- `else` - This is a statement that tells the computer what to do if none of the above conditions were met.

Another important thing to know is that `else if` and `else` statements are completely optional. It's possible to simply write an if statement by itself. It's also possible to write if statements that have no result. Here's an example:

What do you think the computer will do if it's sunny outside?
`if` it is raining, then bring an umbrella

The way this is written, the computer actually will not do anything! You could stop writing here and let the computer do nothing if it's sunny, or you could write more.

`if` it is raining, then bring an umbrella

`else if` it is sunny, then bring sunglasses

Now, the computer will bring sunglasses if it's sunny, but it won't know what to do if it's anything except raining or sunny. So, you might want to add:

`if` it is raining, then bring an umbrella

`else if` it is sunny, then bring sunglasses

`else` stay inside

When it deals with if statements, the computer will run through each statement consecutively, checking for a condition to be met. As soon as it finds a true condition, the computer will complete the task attached to that condition and stop moving down the list. Here's one more example:

What do you think the computer will do if it's noon?

`if` it's morning, eat breakfast.

`else if` it's noon, eat lunch.

`else if` it's evening, eat dinner.

`else` eat a snack!

If you guessed "eat lunch," then congratulations! You've got this all figured out!

## Mini Activity: Automatic Fan

<iframe width="560" height="315" src="https://www.youtube.com/embed/VcuHDy2oJX0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

We are going to write a program that automatically controls an automatic bathroom fan. 

### Setup
For this activity, you will need: 
- Micro:bit
- Micro:bit cable
- Sensor:bit, with the micro:bit plugged in
- Temperature/Humidity sensor, plugged into pin 8 on the sensor:bit

### Step 1: Check humidity numbers
Before we get started on our if statements, let's take a brief moment to refamiliarize ourselves with the range of our humidity sensor. We'll do this the same way we did on Day 1. 
1. In Chrome, go to [MakeCode v4](https://makecode.microbit.org/v4) and create a new project called "automatic fan sensor". 
2. Add the iot-environment-kit extension. Remember, you do this by opening the `Advanced` menu, clicking `Extensions`, searching "iot", and clicking on the extension to add it. 
3. Drag a `serial write value "x" = 0` block from the `Serial` tab into the `forever` block. Then replace "x" with "humidity" and replace the `0` with a `value of dht11 temperature (°C) at pin P15` block from the `Octopus` tab.
4. Change the `value of dht11` block to `humidity (0~100)` and `P8`.

![Humidity logger](/pictures/day1/humidity-logger.png)

5. Connect your micro:bit to the computer, and connect the device in MakeCode. Flash the code to your micro:bit that's connected to the sensor:bit and humidity sensor. Double check that the humidity sensor is plugged into pin 8. Then, click the "Show console device" button to view the humidity graph.

Just like we did on day 1, breathe on the humidity sensor like you're trying to fog it up to raise the humidity level. Then gently blow on it as if you were trying to cool it down to lower the humidity level. Take note of the highest and lowest numbers you see. 

For example, my humidity sensor is currently measuring 44 at the lowest and 97 at the highest. After I "fogged it up", blowing slow cold air helped reduce humidity, but it took a minute or so for the humidity level to return to 45. 

![Humidity up](/pictures/day3/humidity-up.png)
![Humidity down](/pictures/day3/humidity-down.png)

Once you're finished testing, go ahead and move the `serial write...` block outside of the `forever` block so that it's not running, but we can use it later if we need it. 

Now that we know the range of our humidity sensors we can use those numbers to program our automatic fan! *Note: As you are writing `if statements` for this activity, keep in mind that you may need to adjust the numbers to get the program to work right with what your humidity sensor is detecting.*

### Step 2: Turn on the fan when it's humid
We want our fan to automatically turn on when humidity levels are high to prevent mold growth in the bathroom. You could say:

`if humidity level > 70 then turn on the fan` 

1. Let's start by creating a variable for `humidity` and, in the `forever` block, set `humidity` equal to `value of dht11 humidity (0~100) P8`. 
2. `If` blocks and `boolean expression` blocks are in the `Logic` tab. Add an `if` block to your `forever` block, and use a `boolean expression` block to write your coniditonal statement. When you're finished, it should look like this:

![If humidity is high...](/pictures/day3/if-humidity-high.png)

3. Now let's tell the micro:bit what to do when humidity is greater than 70. We want the fan to turn on when this condition is met, so let's use a few `show leds` blocks from the `Basic` tab to create a fan animation. While we're at it, let's pick our favorite `show led` block and place a copy of it in the `on start` block so that we can see our fan as soon as the program starts running. 

![... then turn on the fan](/pictures/day3/led-fan.png)

### Step 3: Turn the fan off when humidity returns to normal
1. Now we need a way to tell the fan to turn off when the humidity level goes back down. We need an `else` block! In the `Logic` tab there is another block that includes an `else` portion, or you can just click the plus sign at the bottom of the `if` block to add an `else` block:

![Add else](/pictures/day3/add-else.png)

2. The blocks within the `else` portion of our `if` block only run if the first condition is **false**. So, if `humidity level > 70` is **true**, the micro:bit will cycle through the fan animation, so fan turns on. If `humidity level < 70` is **false**, the micro:bit will stop changing the leds on the screen, so fan turns off!

![Fan turns on and off](/pictures/day3/fan-on-off.png)

Let's take a moment to test that our code works. Go ahead and flash it to your micro:bit, and then "fog up" the humidity sensor until the fan turns on. Then, blow on the sensor to bring the humidity back down until the fan stops spinning.

### Step 4: Add fan speed settings
With one simple **if** statement, we have made an automatic fan! Let's take it a step further. What if you wanted more than just two options: on and off? We can set fan speeds by adding an `else if` block to our if statement! 

In order to add our fan speed feature, we should do the following:

1. At the bottom of the `if block`, click the plus sign to add an `else if block`

3. We want to test for these three humidity levels (you should adjust these so they are appropriate for the sensor readings you got in step 1): 0-59, 60-79, 80-99. Set the boolean expressions to test for each range:
    - **if** `humidity >= 80`
    - **else if** `humidity >= 60`
    - **else**

Remember, the micro:bit will check each condition in order. The `else if` can can only trigger if the boolean above, `humidity >= 80`, is false. When the computer checks the `humidity >= 60` boolean, we already know that the humidity is less than 80. So, the `else if` will only trigger for humidity levels from 60-79. If the computer makes it all the way down to the `else`, then we know that the number must be less than 80 and less than 60. 

4. Inside the `if` block, show the LEDs in a way that makes the fan spin really fast. You might consider using 2 led blocks instead of 4 for the fast fan animation.

5. Inside the `else if` block, show the LEDs in a way that makes the fan spin slower. You may want to move the `show led` blocks that we used before into this block, but it's up to you! Get creative. 

6. Test out your program by downloading it onto your physical Micro:bit!

Here is a possible [solution](/pictures/day3/finalfan.png) to the automatic light activity, but try your best to figure it out before you look!

## Control Flow: Loops

<iframe width="560" height="315" src="https://www.youtube.com/embed/JsfcdPRoD3A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Another method we have to control the path our program takes is by using **loops**! **Loops** allow us to run the same blocks multiple times. **Loops** also use _conditions_ to determine how many times the code should run in the blocks.

There are several different types of loops available to use in the `Loops` tab, let's talk about a few of them:

#### Repeat Loop

![Repeat Loop](/pictures/day3/repeat.JPG)

The `repeat` loop runs the blocks inside of it the number of times you specify (which can also be a variable).  In this example, a happy face and sad face flash back and forth 4 times.

#### For Loop

![For Loop](/pictures/day3/for.JPG)

The `for` loop is similar to the repeat loop because it also runs the blocks inside of it the number of times you specify. However, `for` loops have a special variable called an `index`. This variable can be named "index," but it does not have to be. Sometimes programmers use "i" or "counter" instead. The `index` value automatically increases at the end of each loop. The loop stops once the desired value is reached. In this example, we display the `index` value which starts at 0 and goes up to 4.

#### While Loop

![While Loop](/pictures/day3/while.JPG)

The `while` loop will keep repeating the blocks inside of it until its **boolean expression** becomes **false**! In the above example, a variable called `counter` starts at `0`, and each time the loop runs its value increases by 1. Each time the loop finishes one loop of code, it checks the condition; if the condition is still **true**, it runs the code again. Eventually, this loop stops when `counter` has the value of `10`. The _condition_ `counter < 10` is **false**, and the loop stops.

#### Forever Block

![Forever Loop](/pictures/day3/forever.JPG)

By now you have noticed that when we start a new program, a `forever` block is in the editor automatically.  The `forever` block is actually just a loop that runs forever! Any blocks that are within the `forever` block will continue to execute as long as the micro:bit has power. The example above will show a beating heart until your micro:bit dies. 


## Activity: Fat and Happy

<iframe width="560" height="315" src="https://www.youtube.com/embed/wITpxJNnIX0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In this activity, we'll pretend that we work at an animal shelter, where our job is to keep a big, orange cat -- who we've named Clark -- fat and happy. It seems like an easy task, all we need to do is keep his water and food dishes full at all times. But, Clark eats and drinks A LOT! 

We'll program our micro:bit to show how much water and food Clark has, and we'll use the `A` and `B` buttons to refill his bowls before they become empty. 

### Setup
For this activity, you will need: 
- 1 micro:bit
- 1 micro:bit cable

### Step 1: Create variables
1. Head to [MakeCode v4](https://makecode.microbit.org/v4) and create a new project. This project will be called "fat-and-happy". 
2. Let's start by creating variables, one for `water` and one for `food`.
3. Set these `water` and `food` variables to 0 in the `on start` block

![Forever Loop](/pictures/day3/water-and-food-variables.png)

<!-- - on-start -->
<!-- - set radio group to 8 -->
<!-- - Create water variable, set to 5 -->
<!-- - create food variable, set to 5 -->

### Step 2: Fill water when `A` is pressed
Let's use the two left columns on the micro:bit screen to represent how much water is available. When we push `A` we want those two columns to light up. 

![LED coordinates first two columns](/pictures/day3/microbit-led-coords-first-two.png)

1. Drag an `on button A pressed` block into the editor. 
2. Create a `set variable` block from the `Variables` and place it inside the `on button A pressed` block. We'll use 5 as the max value because there are 5 rows in the micro:bit LED screen. Change the block so that it says `set water to 5`. 
3. Let's work on turning those LEDs on. We'll start with the first column. Drag five `plot x 0 y 0` blocks into the `on button A pressed` block, and fill them with the appropriate values so that when you press the `A` button, the first column lights up. Your code should look like this: 

![Individually plotted LEDs](/pictures/day3/individually-plotted.png)

4. This works, but it could be better. Instead of using all these repetitive `plot` blocks, we could use a **for loop** with just one `plot` block! Drag the five `plot` blocks we just made out of the `on button A pressed` block. Replace them with a `for index from 0 to 4` block from the `Loops` tab, and put a new `plot x 0 y 0` block inside it.

![Empty for loop](/pictures/day3/empty-for-loop.png)

5. We want this `for` loop to light up five LEDs, which means we need it to run 5 times. You can control how many times a `for index from 0 to 4` block will run by changing the `4`. Right now, our block is already set to run 5 times: once for index 0, 1, 2, 3, and 4, so we'll leave it as is. 
6. We can drag this incrementing `index` into our `plot x 0 y 0` block so that each time the `for` loop runs and the `index` variable increases, it will light up the next LED. Do you think we should use the `index` variable for the x value or the y value? *Hint: look at the old `plot` blocks we set aside.*

![Empty for loop](/pictures/day3/x-or-y.gif)

7. Since the x value isn't changing, we should use the `index` variable for our y value. Go ahead and plug it in. Now, when you click the `A` button, it should do exactly what it did before and light up the first column of LEDs. 

![A button first column](/pictures/day3/a-button-first-column.png)

8. Now that we have our loop set up, lighting up the second column of LEDs is easy! Drag another `plot x 0 y 0` into the `for` loop and fill in the x and y values on your own. When you click the `A` button, the first two columns of LEDs should light up. Try it out, and then check your answers here: [Complete A button](/pictures/day3/a-button-complete.png)

### Step 3: Fill food when `B` is pressed
Great, Clark has water! Now he needs food. We'll set this up the same way that we set up the water, except we want to fill the food when we press the `B` button, and when the food is full, we want the *last* two columns of LEDs to light up. Try to do this on your own, and then come back to check your answers. 

<details>
  <summary>Expand this block for hints!</summary>

> 1. You could copy/paste the entire `on button A pressed` block, and update it for `button B` and water. 
2. Don't forget to change the `food` variable!
3. You will need to change the values in the `plot x 0 y 0` so that the right LEDs light up. *Extra hint: Specifically, the `x` value*

</details>

When you're finished, pressing the `A` button, should light up the first two columns, and pressing the `B` button should light up the last two columns. Your code should look like [this](/pictures/day3/button-a-and-b-done.png). 

### Step 4: Decrease water when Clark drinks
Now that we can fill up the food and water, we should give it to Clark. When Clark takes a drink, the water in the bowl should decrease. We'll say that every now and then, Clark will check the water bowl. If there's water available, then he will drink some. When he takes a drink, the water level will decrease. 

#### Step 4a - Have Clark go to the water at regular intervals
1. To simulate Clark going to the water bowl, we'll use an `every 500 ms` loop from the `Loops` tab. Drag one into the editor.
2. Clark visits his water bowl every 1-3 seconds. (Like we said, he drinks a lot!) Drag a `pick random` block from the `Math` into the `every` block. Set it to `pick random 1000 to 3000`. Now the `every` block will run at random intervals from 1-3 seconds.

#### Step 4b - Set up the `if` block
Whenever Clark goes to the water bowl, he will try to take a drink. Practically, if the bowl is empty, Clark can't take a drink. As far as our program goes, we want to prevent the `water` variable from descending into negative numbers. Create this if statement inside your `every` block:

`if water > 0, then change water by -1`

It should look like this:

![If there's water, take a drink](/pictures/day3/if-then-change-water.png)

In addition to decreasing the `water` variable, we also want the LEDs on the micro:bit to change when the water level goes down. Each time Clark takes a drink, we'll need to turn off two LEDs, one in each water column, to lower the water level on the display. We can use a couple `unplot x 0 y 0` blocks to do this. Add two of these blocks to your `if` block, above the `change water by -1` block. 

#### Step 4c - Set the unplot `x` and `y` values
Let's figure out what x values we need to make our `unplot x 0 y 0` block work right. We already know that all the LEDs in the first column have an x value of `0`, and all the LEDs in the second column have an x value of `1`. So, we'll set x to `0` in the first `unplot` block, and `1` in the second `unplot` block.

![Unplot x values](/pictures/day3/unplot-water-x.png)

Now let's look at the y value. Setting the y values is a bit trickier because they will change based on how much water is in the bowl. 

For example: 
- When Clark takes a drink when `water = 5`, that means that all of the LEDs in the first two columns would be lit. To bring the water level down, we would need to unplot the two LEDs in the top row, which both have a y value of `0`. 
- When Clark takes a drink when `water = 4`, then we need to unplot the LEDs on the next row, which both have a y value of `1`. 

That means that `water` and the `y` values we need to unplot are directly correlated. As the water level goes down, the y value goes up.

![Y and water](/pictures/day3/y-and-water.png)

Because of this correlation, we can use the `water` variable and some math to set our y values. Drag a `0 - 0` block from the `Math` tab to set the y value in both `unplot` blocks to `5 - water`. 

![Unplot y values](/pictures/day3/unplot-y-values.png)

#### Step 4d - Test and check your code
Take a moment to compare your code to the solution....

<details>
  <summary>Fat and Happy Solution Code!</summary>

  ![check your code](/pictures/day3/water-decrease-done.png)
  
</details>

...and test it out in the virtual micro:bit! When you click the `A` button, you should see the water fill up, and then the water should slowly decrease at random intervals. 

### Step 5: Decrease food when Clark eats
We're almost done! All we have to do now is decrease the food in the same way we decreased the water. Try to do this last part on your own, following these general steps:

1. Use another `every` block and set it so Clark eats food every 3-6 seconds (3000-6000 ms). 
2. Remember, Clark can't eat food if the bowl is empty
3. Make sure you decrease the `food` variable whenever Clark eats
4. Use `unplot` blocks and some math to decrease the displayed food level on the micro:bit's LEDs

When you're finished, you can check your answers here: [Complete Fat and Happy Code](/pictures/day3/fat-and-happy-complete.png) Make sure your code is right and that it's working because we'll use it again tomorrow!

### Step 6: Flash your code
That's all there is to it! Flash your code to your micro:bit and test it out. When you press the `A` button, the water should fill up and the two columns on the left side of the screen should light up. When you press the `B` button, the food should fill up and the columns on the right side of the screen should light up. You should see the food and water decrease at semi-random intervals, with the water decreasing faster than the food. 

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/lZwO92iwXBc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Great job today! We learned about **boolean expressions**, expressions that compare two values and always evaluate to **true** or **false**. We learned how to use boolean expressions within **if/else** statements and **loops** to help us _control the flow_ of our program. We used our new knowledge to program an automatic fan and keep a big orange cat named Clark fat and happy. Tomorrow, we'll talk a little bit about functions, and then we'll use everything we've learned so far to program our own micro:pet!
