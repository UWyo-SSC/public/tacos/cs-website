---
id: 'day4'
title: 'Day 4: Functions and Bringing it all Together'
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZrDnydZY-No" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to Day 4! Today, we'll learn about functions and then program our very own `micro:pet`. Our `micro:pet` will respond to its environment, react to attention, get sleepy, and display its happiness using the LEDs on the `micro:bit`. We'll use some of our Octopus sensors discussed along with radio communication to create a pet that needs food and love. We'll also expand on the *Fat and Happy* activity from yesterday to make our micro:pet a corresponding `pet-environment`. 

## Functions

<iframe width="560" height="315" src="https://www.youtube.com/embed/gGcwk5mA-AY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### What is a function?
In computer programming, **functions** provide a way to reuse code and simplify complex blocks. Simply put, they are blocks of code that are given a *name*. We can use this name to to _call_ the function from anywhere in our code. Whenever we call a function, the computer will run whatever block of code is attached to the name we called. 

Functions in code are just like apps in your phone, tablet, or computer. Think about some apps that you use - like the camera, the calculator, and your favorite web browser. What do all these apps have in common? They each complete a certain **task** - for example, your camera app is used only to take pictures and record videos, your calculator app is used to compute math problems, and your web browser can access the internet! This is exactly why we use **functions** in our code - it's a great way to separate different **tasks** that certain code blocks accomplish.

Let's look at an example in MakeCode. If we wanted our micro:bit to show a heart every time any button or pin is pressed, we would need to drag 12 blocks into the editor: 3 `on button pressed` blocks, 3 `on pin pressed` blocks, and 6 `show heart` blocks.

![Show heart program](/pictures/day4/show-heart.png)

After testing our program, we realize that it doesn't do much. Nothing is turning the heart off, so all it does is show a heart continuously, no matter what you do. So, we decide to make it a little more interesting. Let's say we want each button and pin to show a heart, then show a smaller heart, and then turn the LEDs off. We drag 12 more blocks into the editor -- 6 `show small heart` blocks and 6 `clear screen` blocks -- and put them into place. That's 24 blocks.

![Show heart animation](/pictures/day4/show-heart-animation.png)

It's a lot of work to change all of those blocks! Now we're thinking that we want to add an even smaller heart to our animation. Instead of trying to keep track of all the places in our code where we want this heart animation to happen, and manually changing them all the time, we could use a function!

In MakeCode, you can make functions by going to the `Functions` tab. Right now, this tab will be empty because we don't have any functions yet. If we click the `make a Function...` button, a dialogue box appears where you can name your function, and set its parameters. 

![Add function](/pictures/day4/add-function.png)

We'll talk a bit about parameters later. For now, we'll just name our function `show-animation` and click "Done." Now we have a new block in our editor called `Function show-animation`.

![Function block](/pictures/day4/function-block.png)

We'll put our `show icon` blocks and our `clear screen` block inside the function. Now, whenever we _call_ our `show-animation` function, the micro:bit will show a big heart, a small heart, and then clear the screen.

If you look in the `Functions` tab again, you'll see that we have a few more blocks: `return` and `call show-animation`. We'll talk about that `return` block later. Right now, we want to use the `call show-animation` block to replace all the blocks we put in earlier. 

![Function blocks inserted](/pictures/day4/function-blocks-inserted.png)

Even though it looks a bit different, this code does exactly the same thing as it did before, except now, we can easily change our animation and add it to other blocks in our program anywhere we want to. Also, the code looks a lot cleaner and is easier to understand at a glance, making it much easier to work with. Now, all we have to do to add a smaller heart to our animation is add one block instead of 6!

### Parameters and Return Values
The `show-animation` function we discussed earlier was quite simple. However, functions can also take _parameters_ and _return_ values. 

A _parameter_ is an input value for your function, and a _return_ value is the output! If we think about a function like a machine, the parameter is what we put into the machine. Then, the code in our function acts as the gears of the machine, handling the input. Lastly, the output is what the machine produces!

Let's consider an example function called `add(num1, num2)`, which takes two numbers as input and returns their sum. Here's an example of how it would look:

![Add function](/pictures/day4/function-add.png)

In this example, the function `add(num1, num2)` takes two parameters, `num1` and `num2`, and returns a value to the caller (the block that calls the function). As a result, when the `add(num1, num2)` function is called with different arguments, the `micro:bit` will return different sums. You can add a few blocks to use this function so that the `micro:bit` will display: `5`, `7`, and `9`.

![Use variable to show function sum](/pictures/day4/show-sum.png)

It's worth noting that functions can be called from various parts of the code, as demonstrated by calling the `add(num1, num2)` function within the `show number` block. That's pretty neat!

Notice that the `return` block in our example is flat on the bottom. It doesn't have the same notch that other similarly-shaped blocks in MakeCode have. That's because when you call `return` in a function, the function will stop executing at that point. That means you have to be careful when you `return` a value, as all the code after it will not execute! In fact, MakeCode won't even let you place any boxes below a `return` block. 

## Activity: Micro:pet

<iframe width="560" height="315" src="https://www.youtube.com/embed/pWyS1w7lw5c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Have you ever wished for a pet? Maybe you weren't allowed to get one. Or, if you do have one, maybe you share it with your siblings, parents, or guardians. Well today, we're going to make our very own pocket-sized pet, using our microbits and sensors!

### Setup
To make our not so furry friends, you will need:
- 2 micro:bits
- A micro:bit cable
- 1 battery pack
- A sensor:bit (connected to one of the micro:bits)
- UV sensor (plugged into pin 2) 
- Temperature and Humidity sensor (plugged into pin 8)

You will also need the complete [Fat and Happy Code](https://makecode.microbit.org/v4/_MK0gxxP8uYvm) from Day 3. To use this code: 
1. After following this link, click the "edit" button in the top right-hand corner
![Edit code button](/pictures/day4/editcode.png)

2. Before changing any blocks, check for that "v4" in the url, right before `#editor`. If there is no v4, be sure to add it!
![Fat and Happy v4](/pictures/day4/fatandhappyv4.png)


### Step 1: Create and Interact
First, we'll create a basic pet that tracks its happiness and displays a smiley face when it's happy and a frowny face when it's sad. Additionally, we'll set up two inputs so that we can change its happiness and observe the face change on the `micro:bit`.

#### Step 1a - Say hi and create a happiness variable
1. In the `on start` block, use a `show string` block to display "Hi!".
2. Create a `happiness` variable to track your pet's happiness on a scale from 0 to 100. Let's initialize it to 50 by dragging a `set happiness to` block into the `on start` block.

![Hi and Happiness](/pictures/day4/hi_happiness.png)

#### Step 1b - Show your pet's mood
1. Next, we'll show the pet's current happiness using icons. We want our pet to smile when happy and frown when upset, so we need an `if statement` for this.
2. Drag an `every 500ms` block from the `loops` section to your workspace and change the loop repeat time to `1000ms`.
3. Use an `if-then-else` block to display a happy face icon when `happiness` is greater than 50. Otherwise, show a frowny face.

![If/Then/Else](/pictures/day4/if_then_else_smile.png)

#### Step 1c - Program ways to interact with your pet
1. To provide a way to change the pet's happiness and test our program, we'll use the `a+b` button press to represent a hug for our pet.
	- Drag an `on button a+b pressed` block from the `input` section to your workspace.
	- When button `a+b` is pressed, increase `happiness` by 10 and display a heart icon to indicate that your pet is feeling loved.
2. Now, let's introduce something that upsets our pet. Shaking our pet will make it less happy.
	- Drag an `on shake` block from the `input` section to your workspace.
	- When your pet is shaken, decrease `happiness` by 10 and display an angry face icon.

![If/Then/Else](/pictures/day4/happy_sad.png)

#### Step 1d - Test your code
Now, lets test our program. Flash your `micro:bit` with the current program. You can also test this from within Chrome. 
1. It should initially show the text "Hi!". 
2. After that, it should show a frowny face because the `happiness` is not high enough. 
3. So, let's give our pet a hug (press `a+b`)! The pet should display a heart, followed by a happy face.
4. Shake your pet! It should show an angry face, followed by a frowny face because we've lowered our pet's happiness.

Play around with this a bit and see how it works!

#### Code-Check
Your project should now look something like this:

<details>
  <summary>Micro-pet V0 Solution Code!</summary>

  ![Micro:pet v0](/pictures/day4/micropet_v0.png)
  
</details>

### Step 2: Show Mood and Use Sensors
In this section we'll introduce functions and break down the code that shows our pet's mood. We'll also make our pet become a ghost when its happiness reaches 0 or below, and we'll use our temperature and sunlight sensors to further affect our pet's happiness.

#### Step 2a - Change how we show mood
1. First, let's adjust how our pet shows its mood. We'll show different faces based on ranges of happiness. Think back to how you used `if-then-else` blocks for number ranges and to how you used `if-then-else` for number ranges on Day 3. Adjust the code so that: 
	- Show a smiley face if our pet's happiness is *greater than* 50. 
	- Show a frowny face if our pet's happiness is *greater than or equal to* 1 and *less than or equal to* 50. 
	- Show a ghost if our pets happiness reaches 0 or below.

![If/Then/Else/Ghost](/pictures/day4/if_then_else_ghost.png)

#### Step 2b - Add show-mood Function
Now we'll break out the show mood code, which displays faces based on happiness in our `every 1000ms` loop into a function. We're doing this so that we can show mood from various places in the code and keep our loop simple. The best programmers make their code easy to read, understand, and expand on!

1. Create a function named `show-mood` from the `Functions` tab in the `Advanced` dropdown. This is similar to creating a new variable.
2. Let's fill out our new `show-mood` function with the show mood code that displays icons based on `happiness`. 
	- Move the `if-then-else` block that shows the different faces from the `every 1000ms` block to your newly created function `show-mood`.
3. Let's call the `show-mood` function from the `every 1000ms` block so that the mood continues to update as it did previously
	- Drag the `call show-mood` block from the `Functions` tab to your `every 1000ms` block. 
4. Call the `show-mood` function in the `on start` block. 
	- Drag the `call show-mood` block from the `Functions` tab to your `on start` block. 
5. Notice how we can call `show-mood` from two places without having to create a whole new `if-then-else` block. We're reusing code here which is always a good thing!

![Show Mood](/pictures/day4/show_mood.png)

#### Step 2c - Change Pet Happiness with Warmth and Sunlight
Now we are going to use our temperature and UV sensors to adjust our pet's happiness based on the temperature and sunlight. We'll continue to write the code in functions so that we can keep our main loop simple!

First, let's set up the sensors. If you haven't already, click the `Extensions` tab under `Advanced` and add the `iot` extension so we can program using the temperature and sunlight sensors.

![iot](/pictures/day4/iot.png)

You should notice these additional options pop up on MakeCode!

![new options](/pictures/day4/new_options.png)

1. Plug the temperature sensor into `pin 8`. 
2. Plug the sunlight sensor into `pin 2` 

![components assembled](/pictures/day1/sensors-attached-web.jpg)

##### Temperature Function
1. Create a function named `check-temp` from the `Functions` tab in the `Advanced` dropdown. 
2. Our pet doesn't like being too hot or too cold so we'll have it lose happiness if the room temperature is too hot or too cold. The `check-temp` function will make our pet lose happiness if its too cold or too hot. In the function, write code to reduce `happiness` by 1 when the temperature is greater than 85 degrees F or less than 60 degrees F.
	- You'll need an `if-then` block with an `or` block from the `Logic` tab. You'll also need a `value of dht11 at pin` block from the `more` tab under `Octopus`
	- Remember to change the `temperature` to *Farenheit*(F). It defaults to *Celsius*(C).
	- Set the pin to `P8` since that is where our temperature sensor is plugged in.
3. Now that we have this new function. We want to call it after we `show-mood` so that `happiness` is getting updated and the face we display can change. 
	- Drag a `call check-temp` block from the `Functions` tab into your `every 1000ms` block after the `call show-mood` block. 

![check temp](/pictures/day4/check_tmp.png)

##### Sunlight Function
1. Create a function named `check-sunlight` from the `Functions` tab in the `Advanced` dropdown. 
2. The `check-sunlight` function will make our pet gain happiness when its sunny enough outside because our pet likes the sun! In the function, write code to increase `happiness` by 1 when the sunlight value is greater than 25.
	- Just like before You'll need an `if-then` block from the `Logic` tab. You'll also need a `value of light intensity(0-100) at pin` block from  `Octopus` tab.
	- Set the pin to `P2` since that is where our sunlight sensor is plugged in.
3. Just like with `check-temperature`, we want to call our new `check-sunlight` function after we `show-mood` so that `happiness` is getting updated and the face we display can change.
	- Drag a `call check-sunlight` block from the `Functions` tab into your `every 1000ms` block after the `call check-temp` block.

![check sun](/pictures/day4/check_sun.png)

#### Step 2d - Test your Code
- Using the a+b buttons and shaking, make sure your pet shows the three different faces. 
- Try to test sunlight by taking your pet outside and seeing if it gets happier. 
- If you can, try to make your pet too warm and see if it gets unhappy. Don't microwave it!!!!

*Note: You can adjust the temperature values and change in happiness values as needed to make testing a little easier. For example, it might be hard to raise the temperature of your pet to over 85 degrees. It may also be hard to test if the happiness only changes by 1 at a time. You could have happiness change by 10 for testing just to make sure it works.*

#### Code-Check
Your project should now look something like this:

<details>
  <summary>Micro-pet V1 Solution Code!</summary>

  ![Micro:pet v1](/pictures/day4/micropet_v1.png)
  
</details>

### Step 3: Eating and Drinking
Our pet currently responds to its environment. But what about when it gets hungry and thirsty? In this section we'll be building out a capability to consume food and water to keep our pet happy. 

#### Step 3a - Setting up the Pet-Environment
For this section, the first thing we'll need to do is set up the `pet-environment`. Start with the hex file from Day 3 activity: [Fat and Happy Code](https://makecode.microbit.org/v4/_MK0gxxP8uYvm). The environment will display the amount of available food and water and let the pet know how much food and water is available for it to eat and drink.
1. In the `on start` block, use a `radio set group` block to set the radio group to the number you were assigned.
2. Now we'll send the value of water and food to our `micro:pet`. To do this, modify the `on button a pressed` block to send the name and value pair for `water` over the radio. You'll need a `radio send value "name" = 0` block for this. The `"name"` should be `"water"` and the value should be the variable `water`. Hint: you'll want to send the value after its updated in the block.
3. Do the same in the `on button b pressed` block for `food`.

![Micro:pet v2](/pictures/day4/button_press.png)

4. Now that we are sending signals to our `micro:pet`, we need a way to receive a signal for when the pet has consumed food and water! We'll implement this on the pet side in a minute. Drag an `on radio received name value` block to your workspace. 
5. When the received `name = "water"`, we want to unplot the values in column 0 and column 1 by the appropriate amount and change the `water variable`. Oh wait - we've already done this in the `fat-and-happy` code! So let's do some recycling.
	- Using an `if-then` block, drag all the code from the `every pick random 1000 to 2000 ms` block to unplot the LEDs and change the value of `water` when the received `name = "water".` 
6. Rather than always changing the `water` by -1, we want to change water by the `value received` through our radio signalling. Modify the `change water by -1` block to `change water by value`. You can click on the little `value` within your `on radio recieved name value` block to pick it up!

7. Delete the `every pick random 1000 to 2000 ms`.
8. Do the same thing for food and the `every pick random 3000ms to 6000ms` block.

![Micro:pet v2](/pictures/day4/radio_rec.png)

##### Code-Check
Your `pet-environment` V2 code should look like the following:
<details>
  <summary>Pet-environment V2 Solution Code!</summary>

  ![Micro:pet v2](/pictures/day4/pet-environment-solution.png)
  
</details>

#### Step 3b - Consuming Food and Water
It might not look like it, but our pets get hungry! Or at least they will after this step. First we'll build out our pet's ability to eat and drink if there is food and water available. It gets its food and water from the `pet-environment` `micro:bit`, so we'll be using the radio capability to communicate that information. Let's swap back to our Micro-pet code!

1. Since we'll be using the radio, set the radio group to your assigned number by dragging a `radio set group` block to your `on start` block. Put it after the `show string "Hi"` block.
2. Create a variable named `water`, which we'll use to track our pet's available water from 0-5. Let's initialize it to 0 by dragging a `set water to` block into our `on start` block.
3. Create a variable named `food`, which we'll use to track our pet's available food from 0-5. Let's initialize it to 0 by dragging a `set food to` block into our `on start` block.
4. We want to make sure we set up everything up first, then show our mood. So make sure to keep the `show-mood` block at the end of the `on-start` block.

![Micro:pet v2](/pictures/day4/on_start_final.png)

5. Our pet needs a way to actually receive food from its environment. We will accomplish this using the radio receive functionality and setting our `food` and `water` variables accordingly.
	- Drag an `on radio received name value` block to your workspace.
	- Recall that `name` can be different things. We'll need to use an `if-then-else-if` block to do different things depending on the radio signal we receive. 
	- When we receive `name = "water"` set our `water` variable to the `value` received
	- When we receive `name = "food"` set our `food` variable to the `value` received

![recieve food/water](/pictures/day4/recieve_food_water.png)

6. Now that we have `food` and `water` building up from our pet's environment, we are going to allow our `micro:pet` to consume the food with functions. We'll create a function to `eat` and a function to `drink`. Our pet will attempt to do this every 1 second when its checking its environment. If there's food and water our pet will become happier, otherwise it will become less happy. 
	- Create a function named `eat` from the `Functions` tab in the `Advanced` dropdown. 
	- We want our pet to eat only if there's food available. If it does eat, we'll increase the happiness and decrease the available food. If there's no food available, we'll make our pet less happy and ensure the food does not go negative. You'll need an `if-then-else` block for this.
		- In the function `eat`, if `food` is greater than 0, decrease `food` by 1 and increase `happiness` by 1. We decrease the `food` because we used it, and we increase `happiness` to represent our pet being happier from the food.
		- Otherwise, set `food` to 0 and decrease `happiness` by 1. We set `food` to 0 to keep if from ever being negative. You can't have negative food! We already know its not greater than 0. So lets make sure if it did get negative, we correct for that. This makes our food's minimum value 0. We'll create range constraints like this on other variables too.  

		![eat](/pictures/day4/eat_fx.png)
		- Create a function named `drink` from the `Functions` tab in the `Advanced` dropdown. 
		- Implement this function just like food, but do it for the `water` variable. 

		![drink](/pictures/day4/drink_fx.png)
7. Now we need to call `eat` and `drink` in our main loop.
	- Drag a `call eat` block from the `Functions` tab into the `every 1000ms` block.
	- Drag a `call drink` block from the `Functions` tab into the `every 1000ms` block.

	![bigloop](/pictures/day4/bigloop.png)

#### Step 3c - Sending Feedback to the Environment
Since our pet has consumed the food and water, we have to let the pet-environment micro:bit know that happened!
1. In the `eat` function send the a signal that "food" has gone down by 1, after reducing the `food`variable by 1. Do this with a `radio send value name = value` block, where name = "food" and value = -1
2. In the `drink` function send the a signal that "water" has gone down by 1, after reducing the `water` variable by 1. Do this in the same way as you did for food.

![env feedback](/pictures/day4/feedback_env.png)

#### Step 3d - Test your code
- Send food and water to your pet and ensure the environment shows the increasing and decreasing food and water. 
- Check to see if feeding your pet and giving them water makes them happier. 
- Check to see if not feeding them makes them less happy.
Again remember you can change some values to make testing a bit easier. For example, maybe happiness could decrease/increase by 5 when your pet gets food or goes hungry. That way it responds faster to your testing. 

<!-- ### Step 4: Live, Laugh, Love
In this section, we will introduce the concept of "alive" for our pet. Remember how our pet becomes a ghost if its happiness gets too low? We will utilize that concept to create a variable called `alive`. We will use this variable to ensure that the pet only eats, drinks, and performs other activities while it is still alive.

#### Step 4a - Create the Alive Variable and set-alive Function
Here, we will create the `alive` variable and use a function called `set_alive` to manage when it gets set to `true` or `false` based on the current level of happiness. This function will also help us set bounds on happiness to prevent it from exceeding 100 or dropping below 0.

1. Create a variable named `alive`.
2. Since many factors can affect happiness, we'll create a function to manage setting the `alive` variable for us.
    1. Create a function named `set_alive`.
    2. Until now, happiness could go negative. Let's disallow that and set `alive` to `false` when happiness reaches zero. Implement the `set_alive` function as follows:
        1. If `happiness` is less than or equal to 0, set `happiness` to 0 (to prevent negative values, similar to `water` and `food`) and `alive` to `false`. Now we need to be cautious about our pet's happiness.
        2. If happiness is not less than or equal to 0, set `alive` to `true`.
        3. If `happiness` is greater than 100, set `happiness` to 100 (to establish an upper bound on happiness).
		![if_alive](/pictures/day4/if_alive.png)
3. In our main loop (`every 1000ms` block), use an `if-then` block to ensure that our pet is `alive` before performing all the checks, such as eating, drinking, and other activities, but still display the mood.

![if_alive_then](/pictures/day4/if_alive_then.png)

#### Step 4b - Modify show-mood
Now, we'll modify our `show_mood` function to display the ghost face when our pet is not `alive` and add some additional faces to make our pet more expressive. Feel free to use the built in icons, or use a basic `show leds` and make your own faces!

1. Since we now have the `alive` variable, we can use it to immediately show the ghost face.
2. Remember that functions allow you to use `return` to skip the rest of the function and execute the next block. We can take advantage of this by adding a block at the beginning of `show_mood` to display the ghost icon and return if our pet is no longer `alive`.
    1. Before that, we need to ensure that the `alive` variable is set. Fortunately, we have a function for that. Call `set_alive` at the beginning of the `show_mood` function. In most places where we use the `alive` variable, we'll need to call `set_alive` first. Note: We don't need to add it in the `every 1000ms` loop because we are calling it here, and the first thing we do in that loop is call `show_mood`.
    2. Right after that, add an `if-then` block to display the ghost icon and `return` if our pet is not `alive`. You can find the `return` block in the `Functions` tab.

	![return](/pictures/day4/return.png)

3. Let's modify the faces our pet will display at different levels of happiness. We will add a few more faces to make it more interesting. Remember from Day 3 that for number ranges, since we are using `else-if` blocks, we don't have to check the range each time. We can simply go in descending order using the greater-than symbols. You are free to change these numbers and even add more ranges to make your pet more expressive.
    1. If `happiness` is greater than or equal to 100, display a super happy face!
    2. If `happiness` is between 75 and 100, display a mostly happy face.
    3. If `happiness` is between 50 and 75, display a smiley face.
    4. If `happiness` is between 25 and 50, display a sad face.
    5. Otherwise, display an angry face.

#### Step 4c - Use alive in a+b and on shake
Since we have the `alive` variable, we'll use it in our `on a+b button pressed` and `on shake` blocks. Remember that pressing `a+b` represents hugging our pet, and shaking the pet makes it upset! We need to add the `alive` variable here because our pet cannot accept our hug or get angry at us for shaking it if it is no longer alive.

1. Modify the `on shake` block:
    1. Use an `if-then` block to only make the pet get angry and change `happiness` if it is `alive`.
    2. Remember to call `set_alive` before checking the `alive` variable!
2. Modify the `on a+b button pressed` block:
    1. Use an `if-then` block to only make the pet happy and change `happiness` if it is `alive`.
    2. Remember to call `set_alive` before checking the `alive` variable!

![love hate](/pictures/day4/love_hate.png) -->

### Step 4: Test Your Code
<!-- - Now that there's an alive variable, we'll want to make sure that if our pet gets too unhappy, the ghost face shows and no matter what we can't bring it back.  -->
#### Final Code-Check
Great work! At this point, your project should look something like this **BUT** keep in mind that your radio group in the `set radio group` blocks should be set to your **assigned radio group**! 

<details>
  <summary>Micro-pet V2 Solution Code!</summary>

  ![Micro:pet v3](/pictures/day4/micropet-v2-transparent.png)
  
</details>

And your `pet-environment` should look something like this:

<details>
  <summary>Pet-environment V2 Solution Code!</summary>

  ![Micro:pet v3](/pictures/day4/micropet-v2-env.png)
  
</details>

#### Test Your Final Code
Flash your Micro:Pet to the micro:bit that is connected to your sensors, and flash your Pet:Environment code to the other. 
- Make your pet unhappy by not feeding it, or by shaking it and ensure that it eventually becomes a ghost.
- Also try and get your pet to show all the different faces. Make it really happy, then ignore it for a while and watch is get more and more unhappy.

#### Stuck? 
Are you stuck? Go back through the step-by-step guide and screenshots and give debugging your best shot. Still stuck? Ask questions!! :)

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/PGGyFx4tPb4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Good job! Today, we learned about functions and how they help us reuse and simplify our code. Then, we created a `micro:pet` that interacts with a `pet-environment` to eat and drink, gets happy when it's hugged, and gets angry when it's shaken. The pet will show different faces depending on how happy it is and even becomes a ghost if it becomes too unhappy.  Tomorrow, we'll expand on our `micro:pet` code by introducing a `sleepiness` variable and having it eventually fall asleep depending on its environment. We'll also make use of more functions to keep our code neat, clean, and organized!