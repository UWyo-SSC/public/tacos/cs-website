---
id: 'day5'
title: 'Day 5: Further Exploration'
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/Cv08YKD94Z0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Welcome to Day 5 of TACoS: Computer Science! Today, we'll work together some more on our `micro:pet` code. We'll add some additional features and clean up the code with more functions. Then we'll talk about how you can keep going on your own!

## Activity: Expand Micro:pet

<iframe width="560" height="315" src="https://www.youtube.com/embed/fYVBxW2St94" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In this activity, we will expand the capability of our `micro:pet` to become sleepy and fall asleep under certain conditions. By the end of the day, we'll be able to put our pet to bed for the night, preventing their happiness from decreasing. We'll use a couple more functions to simplify the coding, and we'll also implement a couple of ways to wake our pet up.

### Setup
For this activity, you will need:
- 2 micro:bits
- A micro:bit cable
- 1 battery pack
- A sensor:bit (connected to one of the micro:bits)
- Noise sensor (plugged into pin 1)
- UV sensor (plugged into pin 2) 
- Temperature and Humidity sensor (plugged into pin 8)
- [Your finished micro-pet code from Day 4](https://makecode.microbit.org/_b1z3ooWoo2fh)
- [Your finished micro-pet environment code from Day 4](https://makecode.microbit.org/_YdsJ19Rgi23J)

### Step 1: Catch Some Z's 
In this section, we'll add the `sleepiness` variable, which is similar to the `happiness` variable. It will keep track of how sleepy our pet is, and when it reaches 10, our pet will fall asleep. We'll also introduce another variable called `awake`. This will make it easy to check and keep track of whether our pet is awake.

#### Step 1a - Add Sleepiness
1. Create a variable named `sleepiness` and initialize it to 0 in the `on start` block.

![check sun](/pictures/day5/sleeping.png)

2. Let's find ways to adjust our pet's sleepiness. First, we'll make our pet less sleepy when they are out in bright sunlight. However, we still want our pet to become happier in the sunlight. Modify `check-sunlight` to behave as follows:
    - If the sunlight intensity is greater than 50, reduce `sleepiness` by 1.
    - If the sunlight intensity is greater than 25, increase `happiness` by 1.

![check sun](/pictures/day5/check_sun.png)

#### Step 1b - Modify Interactions
1. Remember that pushing `a+b` represents hugging our pet, which already makes our pet happier. Let's also make it remove all `sleepiness`.
    - In the `on buttona+b pressed` block, set `sleepiness` to 0
2. Remember how shaking our pet makes it angry? It should probably also wake it up.
    - In the `on shake` event, set `sleepiness` to 0

![shake/hug sleepiness](/pictures/day5/sleepiness_shake_hug.png)

#### Step 1c - Awake
1. Create a variable named `awake`. Just like with `happiness`, many things can affect `sleepiness`, so we'll create a function to manage setting `awake` for us.
    - Create a function called `set-awake`.
    - Set bounds for `sleepiness` between 0 and 10. If `sleepiness` is less than 0, set it to 0 and set awake to true.
    - If `sleepiness` is greater than or equal to 10, set `sleepiness` to 10 and set awake to false.
    <!-- - If `sleepiness` is greater than or equal to 10, set `awake` to false. Otherwise, set `awake` to true. -->

![set-awake function](/pictures/day5/step1c_awake.png)

2. Now let's make use of our new `awake` variable. We want our pet to only `eat` and `drink` when it is `awake`. Modify the `every 1000ms` block to include the conditions for `awake` before allowing the pet to eat and drink.
    - We still want to `check-temp` and `check-sunlight` even if our pet is not awake.

![eat and drink if awake](/pictures/day5/eat_and_drink_awake.png)


#### Step 1d - Sleepy Face
1. Modify the `show-mood` function. If our pet is not `awake`, show a sleepy face and `return`. Add an `if-then` block near the top to change the face, then `return`. Remember to call `set-awake` before checking the variable!

![awake](/pictures/day5/show_mood_pt.png)

### Step 2: Break out Some Functions
In this section we'll encapsulate the idea of hugging, waking-up and putting our pet to bed with functions. We'll also make use of the noise sensor to wake our pet up when it gets too loud in the room.

#### Step 2a - Hugs 
We've been using `a+b` to represent hugging our pet, but to someone who just looks at this code, that may not be obvious. So we'll break out the code within the `on a+b button pressed` block into a `hug` function.

1. Create a function named `hug`.
2. Move the blocks from within the `on button a+b pressed` block into the function.
3. Call `hug` from the `a+b button pressed` block.
4. Quick Code Check. Here's what that will look like.


![Hug function](/pictures/day5/hug_function.png)

#### Step 2b - Wake-Up 
Now we'll do the same thing for waking our pet up.
1. Create a function named `wake-up`.
2. Move the blocks from within the `on shake` block into the function.
3. Call `wake-up` from the `on shake` block

#### Step 2c - Go to Bed 
Let's add the capability to put our pet to bed.
1. Drag an `on button b pressed` block to your workspace.
2. Create a function named `go-to-bed`.
    - Implement the function `go-to-bed` to set `sleepiness` to 10.
3. Call `go-to-bed` from the `on button b pressed` block

![Go to bed function](/pictures/day5/go_to_bed.png)

#### Step 2d - Light Sleeper
Finally, we will use the noise sensor to wake our pet up without angering it. We'll also show a surprised face when it gets too loud. You'll need the `value of noise(db)` block from the `Octopus` tab and an `if-then` block.
1. Connect the noise sensor to pin 1.
2. Create a function named `check-noise`.
    - Implement `check-noise` so that if the noise is greater than or equal to 60, set the `sleepiness` to 0 and show a surprised face icon.
3. We want the sound to be continually checked, so we'll use a `forever` block.
    - Drag a `forever` block from the `Basic` tab onto your workspace.
    - Call `check-noise` within the forever block

![Check noise](/pictures/day5/check_noise.png)

### Step 3: Test Your Code
- Make sure your pet interactions change both the `happiness` and the `sleepiness` variables.
- Make your pet go to sleep, then wake it up.

#### Code-Check
Your project should look something like this. Whoa, that's a lot of code! Right click the image, and select "Open in new tab". Then, click on the image to zoom in!

<details>
  <summary>Micro-pet Final Solution Code!</summary>

  ![final](/pictures/day5/final_pet.png)
  
</details>

## Further Exploration

<iframe width="560" height="315" src="https://www.youtube.com/embed/Royxsi_S-hU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

We've put a lot of work into our micro:pets, but there's still a lot of room to change and improve! Take a moment to brainstorm some additional features you could add. Here are a few ideas:
- Use the `music` feature to make your pet make sounds along with faces.
- Add new variables similar to `happiness` and `sleepiness` to make your pet more realistic.
- Make another pet and create interactions between two.
There are lots of avenues for expansion, so use your imagination, do some research, or ask a friend to help you come up with ideas! If you want some motivation for why to keep modifying your pet, think about how many things continuously evolve and improve - phones, computers, code, and even *you*!

## Conclusion

<iframe width="560" height="315" src="https://www.youtube.com/embed/A5nMHyW88Kc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Today, you practiced using functions to make your code clean and readable. You added some new functionality to your micro:pet, and hopefully brainstormed some extra things to add on to it later. Way to go! 

Thank you for joining us on this TACoS adventure! Hopefully, you had fun trying things out on your own - we bet you came up with some pretty neat ideas! Don't forget to show off your projects to your friends and family, and remember, the possibilities for what you can create are endless. Don't stop here. Keep exploring and trying things, who knows what you'll make next! 
