---
title: TACoS 2023 - Computer Science
hide_table_of_contents: true
---


## Agenda
### [Day 1](Day1.md) - Intro to Computer Science and the Micro:bit<br />
### [Day 2](Day2.md) - Variables and How Micro:bits Communicate<br />
### [Day 3](Day3.md) - Boolean Expressions and Control Flow<br />
### [Day 4](Day4.md) - Functions and Bringing it all Together<br />
### [Day 5](Day5.md) - Further Exploration<br />


