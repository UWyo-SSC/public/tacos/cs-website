module.exports = {
  title: 'TACoS 2023: Computer Science',
  tagline: '',
  url: 'https://uwyo-ssc.gitlab.io',
  baseUrl: '/public/tacos/cs-website/',
  favicon: 'img/seed.png',
  organizationName: 'UWyo-SSC', // Usually your GitHub org/user name.
  projectName: 'cs-website', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'TACoS 2023',
      logo: {
        alt: 'My Site Logo',
        src: 'img/seed.png',
      },
      items: [
        {
          href: 'https://gitlab.com/UWyo-SSC/public/tacos/cs-website',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Connect',
          items: [
            {
              label: 'Email',
              href: 'mailto:andrea.burrows@ucf.edu,mike.borowczak@ucf.edu',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/WyCS307',
            },
          ],
        },
        {
          title: 'Learn More',
          items: [
            {
              label: 'WyCS',
              href: 'https://www.uwyo.edu/wycs/',
            },
            {
              label: 'CEDAR Lab',
              href: 'https://uwcedar.github.io/digiflyer#slide=1',
            },
            {
              label: 'GenCyber',
              href: 'https://www.cs.uwyo.edu/~mborowcz/camps/cowpokes/',
            },
          ],
        },
        {
          title: 'Makecode Resources',
          items: [
            {
              label: 'Blocks Documentation',
              href: 'https://makecode.microbit.org/blocks',
            },
            {
              label: 'Makecode FAQ',
              href: 'https://makecode.microbit.org/faq',
            },
            {
              label: 'Makecode Docs',
              href: 'https://makecode.microbit.org/docs',
            },
          ],
        },
        {
          title: 'More Micro:bit Material',
          items: [
            {
              label: 'Intro CS w/ Micro:bit',
              href: 'https://makecode.microbit.org/v0/--docs#book:/courses/csintro/SUMMARY',
            },
            {
              label: 'Python User Guide',
              href: 'https://microbit.org/get-started/user-guide/python/',
            },
            {
              label: 'Javscript Tutorial',
              href: 'https://makecode.microbit.org/courses/blocks-to-javascript/hello-javascript',
            },
          ],
        },
      ],
      copyright: `Website built by the University of Wyoming CEDAR Lab with Docusaurus. Funding provided by MilliporeSigma, NSA GenCyber program and INL.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/UWyo-SSC/public/tacos/cs-website',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/UWyo-SSC/public/tacos/cs-website',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
