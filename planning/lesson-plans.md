# TACoS Lesson Plans
## Day 1 - Introduction to Computer Science and the Micro:bit
- What is Computer Science?
- What is a Micro:Bit?
- Activity: Hello Micro:bit
- Sensors
- Mini Activity: Noise Sensor

## Day 2 - Variables
- What is a variable?
- Activity: TODO about variables
- How do Micro:bits communicate? 
  - Name-value pairs
- Activity: TODO 

## Day 3 - Boolean Expressions and Control Flow
- Boolean Expressions
- Control Flow 
- If Statements
- Activity: TODO
- Loops
- Activity: TODO

## Day 4 - Bringing it all together
- Functions
- Mini Activity: Functions
- Activity: Micro:pet

## Day 5 - Further Exploration
- How to make your micro:pet code even better
  - Ideas and a couple how-to's for expansion (add sound)
- Build it out, physically (attach micro:bit and sensors to a cardboard creature)

## Need to incorporate:

