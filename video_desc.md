# Table of Contents

1. [Day 1](#day-1)
   - [Intro 1](#intro-1)
   - [What is Computer Science](#what-is-computer-science)
   - [What is a Micro:bit](#what-is-a-microbit)
   - [Activity: Hello Micro:bit](#activity-hello-microbit)
   - [Sensor:bit and Octopus Sensors](#sensorbit-and-octopus-sensors)
   - [Activity: Sensor Test](#activity-sensor-test)
   - [Conclusion 1](#conclusion-1)
2. [Day 2](#day-2)
   - [Intro 2](#intro-2)
   - [Variables](#variables)
   - [Activity: Fruit Prices](#activity-fruit-prices)
   - [How do Micro:bits Communicate](#how-do-microbits-communicate)
   - [Activity: Chaos Simulator](#activity-chaos-simulator)
   - [Conclusion 2](#conclusion-2)
3. [Day 3](#day-3)
   - [Intro 3](#intro-3)
   - [Boolean Expressions](#boolean-expressions)
   - [Control Flow: If Statements](#control-flow-if-statements)
   - [Mini Activity: Automatic Fan](#mini-activity-automatic-fan)
   - [Control Flow: Loops](#control-flow-loops)
   - [Activity: Fat and Happy](#activity-fat-and-happy)
   - [Conclusion 3](#conclusion-3)
4. [Day 4](#day-4)
   - [Intro 4](#intro-4)
   - [Functions](#functions)
   - [Activity: Micro:pet](#activity-micropet)
   - [Conclusion 4](#conclusion-4)
5. [Day 5](#day-5)
   - [Intro 5](#intro-5)
   - [Activity: Expand Micro:pet](#activity-expand-micropet)
   - [Further Exploration](#further-exploration)
   - [Conclusion 5](#conclusion-5)

# Day 1
## Intro 1
Welcome to Day 1 of Computer Science with TACoS! This video shows what you'll need to get started with Day 1 activities. 

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1
## What is Computer Science
Watch this video to learn the definition of computer science

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1
## What is a Micro:bit
Watch this video to learn about the Micro:bit

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1
## Activity: Hello Micro:bit
This is our first activity with the Micro:bit! By the end of this video, you will be able to program your Micro:bit to tell you "hello" and "goodbye."

TACoS Website: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1

Start coding at: https://makecode.microbit.org/v4
## Sensor:bit and Octopus Sensors
Watch this video to learn about the new sensors we will use throughout the week

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1

Harward Documentation
- Elecfreaks Sensor:bit https://www.elecfreaks.com/learn-en/microbitExtensionModule/sensor_bit.html
- Elecfreaks Octopus Analog Noise Sound Sensor Detection Module https://www.elecfreaks.com/learn-en/microbitOctopus/sensor/octopus_ef04081.html
- Elecfreaks Octopus Analog UV sensor https://www.elecfreaks.com/learn-en/microbitOctopus/sensor/octopus_ef04093.html 
- Elecfreaks Octopus Temperature And Humidity Sensor https://www.elecfreaks.com/learn-en/microbitOctopus/sensor/octopus_ef04019.html
## Activity: Sensor Test
This is our first activity with the new Octopus sensors! By the end of this video, you will be familar with programming the new sensors.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1
## Conclusion 1
This video shows a summary of all the activities and lessons completed in Day 1. Great job on your first day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day1

# Day 2
## Intro 2
Welcome to Day 2 of Computer Science with TACoS! This video shows what you'll need to get started with Day 2 activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day2
## Variables
Watch this video to learn about variables.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day2
## Activity: Fruit Prices
This is our first activity with variables! By the end of this video, you will be able to program your Micro:bit to calculate the price of fruit!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day2

Start coding at: https://makecode.microbit.org/v4
## How do Micro:bits Communicate
Watch this video to learn about how Micro:bits communicate with each other.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day2
## Activity: Chaos Simulator
This is our first activity with Micro:bit communication! By the end of this video, you will be able to program your Micro:bit to communicate with other Micro:bits and restore the universe to a state of peace and tranquility. 

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day2

Start coding at: https://makecode.microbit.org/v4
## Conclusion 2
This video shows a summary of all the activities and lessons completed in Day 2. Great job on your second day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day2

# Day 3
## Intro 3
Welcome to Day 3 of Computer Science with TACoS! This video shows what you'll need to get started with Day 3 activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day3
## Boolean Expressions
Watch this video to learn about boolean expressions.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day3
## Control Flow: If Statements
Watch this video to learn about controlling the flow of your program with if statements.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day3
## Mini Activity: Automatic Fan
This is our first activity with if statements! By the end of this video, you will be able to program your Micro:bit to turn on/off a fan based on humidity.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day3

Start coding at: https://makecode.microbit.org/v4
## Control Flow: Loops
Watch this video to learn about loops and the different types we can program with.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day3
## Activity: Fat and Happy
This is our first activity with loops! By the end of this video, you will be able to program your micro:bit to show how much water and food our pet has. We will also program a way to refill its bowls before they become empty. 

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day3

Start coding at: https://makecode.microbit.org/v4
## Conclusion 3
This video shows a summary of all the activities and lessons completed in Day 3. Great job on your third day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day3

# Day 4
## Intro 4
Welcome to Day 4 of Computer Science with TACoS! This video shows what you'll need to get started with Day 4 activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day4
## Functions
Watch this video to learn the what, why, and how of programming with functions.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day4
## Activity: Micro:pet
This is our first activity with functions! By the end of this video, you will program your very own pocket-sized pet, using our microbits and sensors!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day4

Start coding at: https://makecode.microbit.org/v4

Fat and Happy Code: https://makecode.microbit.org/v4/_MK0gxxP8uYvm
## Conclusion 4
This video shows a summary of all the activities and lessons completed in Day 4. Great job on your fourth day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day4

# Day 5
## Intro 5
Welcome to Day 5 of Computer Science with TACoS! This video shows what you'll need to get started with Day 5 activities.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day5
## Activity: Expand Micro:pet
In this video, we will expand the capability of our `micro:pet` to become sleepy and fall asleep under certain conditions. By the end of the day, we'll be able to put our pet to bed for the night, preventing their happiness from decreasing. We'll use a couple more functions to simplify the coding, and we'll also implement a couple of ways to wake our pet up.

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day5

Start coding at: https://makecode.microbit.org/v4
## Further Exploration
In this video, we'll talk about how you can keep going on your own!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day5
## Conclusion 5
This video shows a summary of all the activities and lessons completed in Day 5. Great job on your last day!

TACoS site: https://community.pages.ssc.dev/wycs/tacos/2023/cs-website/docs/day5




